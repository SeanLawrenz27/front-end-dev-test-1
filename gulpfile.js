var gulp = require('gulp'),
sass = require('gulp-sass'),
sourcemap = require('gulp-sourcemaps'),
del = require('del'),
concat = require('gulp-concat'),
uglify = require('gulp-uglify'),
rename = require('gulp-rename');

//SASS compiling 
gulp.task('compileSass', function(){
	return gulp.src('scss/styles.scss')
	.pipe(sourcemap.init())
	.pipe(sass())
	.pipe(sourcemap.write('./'))
	.pipe(gulp.dest('public/css'));
});

gulp.task('watchFiles', function(){
	gulp.watch('scss/**/*.scss', ['compileSass']);
});

gulp.task('clean', function(){
	del(['dist', 'public/css/styles.css', 'public/js/app.js']);
});

gulp.task('build',['compileSass'], function(){
	return gulp.src(['public/css/styles.css', 'public/js/app.js', 'index.html'], {base: './'})
		.pipe(gulp.dest('dist'));
});

gulp.task('serve', ['watchFiles']);

gulp.task('default', ['clean'],function(){
	gulp.start('build');
});