//Mother Object
var Lightbox = function(type,href){
	this.el = document.querySelector('.lightbox');
	this.body = document.querySelector('.lightbox .body');
	this.content = document.querySelector('.lightbox .lightbox-content');
	this.type = type;
	this.href = href;
	this.image = null;
	this.video = null;

	this.width = window.innerWidth * .7;
	this.height = window.innerHeigth * .5;
	//Proto Initalizer
	this.init();
}

Lightbox.prototype ={
	//Initalizer
	init : function(){
		var _this = this;

		// DOM Builder
		if(!this.el)
			this.create();

	},
	create: function(){
		var _this = this,
		cl = document.createElement('div'), // close x
		bd = document.createElement('div');// backdrop 


		this.el = document.createElement('div');
		this.content = document.createElement('div');
		this.body = document.createElement('div');

		this.el.classList.add('lightbox');
		bd.classList.add('backdrop');
		cl.classList.add('close');
		this.content.classList.add('lightbox-content');
		this.body.classList.add('body');

		cl.innerHTML = '<i class="fa fa-times"></i>';

		this.el.appendChild(bd);
		this.content.appendChild(cl);
		this.content.appendChild(this.body);
		this.el.appendChild(this.content);
		document.body.appendChild(this.el);

		cl.addEventListener('click', function() {
			_this.close();
		});

		//I was having issues with a video stying on the screen. So I added a transisiton listener to remove the url of the video.
		var f = function(e) {
			if (_this.isOpen()) return;
			_this.el.classList.remove('show');
			_this.body.innerHTML = '';
		}

		this.el.addEventListener('transitionend', f, false);
		this.el.addEventListener('webkitTransitionEnd', f, false);
		this.el.addEventListener('mozTransitionEnd', f, false);
		this.el.addEventListener('msTransitionEnd', f, false);
	},
	loadImage: function(){
		var _this = this;

		//this.setDimensions(this.width, this.height);

		if(!this.image){
			this.image = new Image();
			this.image.src = this.href;
		}
		this.body.appendChild(this.image);
	},
	loadIframe: function(){
		this.body.innerHTML = '<iframe src="' +this.href+ '" frameborder="0" allowfullscreen></iframe>';
	},
	open: function(){
		//Intelligentlly deciding the type Image vs Video
		switch(this.type){
			case 'image':
				this.loadImage();
				break;
			case 'iframe':
				this.loadIframe();
				break;
			case 'carousel':
				this.loadImage();
				break;
			default: 
				this.loadIframe();
		}

		this.el.classList.add('show');
		//force render
		this.el.offsetHeight; 
		this.el.classList.add('open');
	},
	close: function(){
		this.el.classList.remove('open');
		this.href = '';
	},
	isOpen: function(){
		//Part of that remove tranisition thing.
		return this.el.classList.contains('open');
	}
}

function carousel(imageTracker){
	var images =['public/img/slideshow/stoneland-gallery-1.jpg','public/img/slideshow/stoneland-gallery-2.jpg','public/img/slideshow/stoneland-gallery-3.jpg','public/img/slideshow/stoneland-gallery-4.jpg','public/img/slideshow/stoneland-gallery-5.jpg'],
	imageTracker = imageTracker;

	var media = new Lightbox('carousel', images[imageTracker]);
	//Adding chevrons
	media.backArrow = document.createElement('div');
	media.backArrow.classList.add('chevron-arrow-backward')
	media.forwardArrow = document.createElement('div');
	media.forwardArrow.classList.add('chevron-arrow-forward');
	media.body.appendChild(media.backArrow);
	media.body.appendChild(media.forwardArrow); 

	//adding listeners
	media.backArrow.addEventListener('click', function(){
		if(imageTracker === 0){
			var endOfImages = images.length - 1;
			media.close();
			media.el.classList.remove('show');
			media.body.innerHTML = '';
			carousel(endOfImages);
		}else{
			media.close();
			media.el.classList.remove('show');
			media.body.innerHTML = '';
			imageTracker = imageTracker - 1;
			carousel(imageTracker);
		}
	});

	media.forwardArrow.addEventListener('click',function(){
		var endOfImages = images.length - 1;
		if(imageTracker === endOfImages){
			media.close();
			media.el.classList.remove('show');
			media.body.innerHTML = '';
			carousel(0);
		}else{
			media.close();
			media.el.classList.remove('show');
			media.body.innerHTML = '';
			imageTracker = imageTracker + 1;
			carousel(imageTracker);
		}
	});
	media.open();
}




